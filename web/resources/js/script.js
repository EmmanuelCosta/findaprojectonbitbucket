$(document).ready(
    /* This is the function that will get executed after the DOM is fully loaded */
    function (e) {
        datePicker();
        $("#loading-div-background").css({ opacity: 1.0 });

        var divClone = $("#resultDiv").clone();

        $("#submit").click(function () {
            showProgressAnimation();
            $("#resultDiv").replaceWith(divClone.clone());
            var language = $('#language').val();
            var startdate = $('#startdate').val();

            if (startdate == "" || language == "") {
                alert("Please provide a value for the language and the date");
            } else {
                $.ajax({
                    type: "GET",
                    url: "SearchServlet",
                    dataType: 'json',
                    async: true,
                    data: {
                        language: language,
                        startdate: startdate
                    },
                    success: function (data) {
                        hideProgressAnimation();
                        var response = $(data);
                        $("#resultDiv").css("display", "");
                        var repos = data.Repos;

                        console.log(repos+"  == ");
                        $.each(JSON
                            .parse(repos.values), function (idx, resultValue) {
                            console.log(resultValue.owner + " " + resultValue.language + " " + resultValue.links + " " + resultValue.description);
                            var tablerow = "<tr>" +
                                "<td>" + resultValue.owner + "</td>" +
                                "<td>" + resultValue.repoName + "</td>" +
                                "<td>" + resultValue.language + "</td>" +
                                "<td>" + resultValue.description + "</td>" +
                                "<td><a href=\"" + resultValue.links + "\">click here to open repo page</a></td>" +
                                "</tr>";
                            $("tbody").append(tablerow);

                        });

                    },
                    error: function (data, status, error) {
                        alert("data: " + data.status + " status: " + status.status + " error : " + error + " error status " + error.status);

                    }
                });
            }
            //this to prevent form refresh page and cancel ajax call
            //
            return false;

        });

        //AUTO COMPLETE
        var $autocompletLang = $('#language');
        $autocompletLang.autocomplete({
            open: function () {
                $(this).autocomplete('widget').css('z-index', 9999);

                return false;
            },
            source: function (request, response) {
                var prefixeWord = request;

                console.log(prefixeWord);
                $.ajax({
                    type: "GET",
                    url: "AutoCompleteLanguageServlet",
                    dataType: 'json',
                    data: request,
                    success: function (data) {
                        $.each(data, function (idx, resultValue) {
                            response(resultValue);
//                            for (var i = 0; i < resultValue.length; i++) {
//                                console.log(resultValue[i]);
//                            }
                        });
                    }
                })
            }
        });
    }
);

function datePicker() {
    $(".datepicker").datepicker({
        format: "yyyy-mm-dd"
    });
}

function showProgressAnimation(){
    $("#loading-div-background").show();
}

function hideProgressAnimation(){
    $("#loading-div-background").hide();
}