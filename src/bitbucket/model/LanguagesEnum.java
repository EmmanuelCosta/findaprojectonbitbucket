package bitbucket.model;

/**
 * Created by ebabalac on 07/07/2015.
 */

//TODO GET THE VALUES OF AVAILABLE LANGUAGE DIRECTLY FROM BITBUCKET
public enum LanguagesEnum {

    ALL("ALL"),JAVA("JAVA"), ANDROID("ANDROID"), DOTNET(".NET"), CSHARP("C#"), MARKDOWN("MARKDOWN"), VIM("VIM"), C("C"), CPLUSPLUS("C++"), RUBY("RUBY");
    private String value;

    private LanguagesEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
