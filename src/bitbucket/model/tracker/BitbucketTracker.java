package bitbucket.model.tracker;

import bitbucket.parser.JSONRepoParser;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emmanuel on 25/06/2015.
 */


public class BitbucketTracker {

    private static String bitbucketAPItURL = "https://bitbucket.org/api/2.0/";
    private List<JSONRepoParser> pagerList;
    private int retry;

    public BitbucketTracker() {
        this.pagerList = new ArrayList<JSONRepoParser>();
        retry = 0;
    }

    /**
     * this will use the api 2.0 of bitbucket on constructing a correct GET Method
     * it will take Repo from bitbucket
     *
     * @param url   : can be null or empty. If it is so, the default url will be https://bitbucket.org/api/2.0/repositories/
     * @param after : can be null or empty. it is the date from which the repo can be recovered.
     * @throws IOException
     * @throws ParseException
     */
    public List<JSONRepoParser> getJSONParserList(String url, String after, String language) throws IOException, ParseException {
        try {
            String repoStr = constructURL(url, after);
            URL repoURL = new URL(repoStr);
            HttpURLConnection conn = (HttpURLConnection) repoURL.openConnection();
//        System.out.println(repoURL.toString());
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() == 200) {
                retry = 0;
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

                String output;
//        System.out.println("Output from Server .... \n");
                // we do it like this because the server give all we need in the first line
                output = br.readLine();
                conn.disconnect();

                JSONRepoParser jsonRepoParser = new JSONRepoParser();
                System.out.println(output);
                jsonRepoParser.parse(output, language);
                List<Repo> repoList = jsonRepoParser.getRepoList();
                if (repoList.size() > 0)
                    pagerList.add(jsonRepoParser);
                String nextPage = jsonRepoParser.getNextPage();
                if (nextPage != null && !nextPage.isEmpty()) {
                    //changes this by getJSONParserList(nextPage,after)
                    // this will make method recursive and get all result macthing your need
                    if (pagerList.size() > 10) {
                        return pagerList;
                    } else {

//                    Logger.getAnonymousLogger().info(jsonRepoParser.toJSONString());
                        Thread.sleep(2000);
                        return getJSONParserList(nextPage, after, language);
                    }
                }
            } else if (conn.getResponseCode() == 429) {

                // we have been rejected because of the frequency of request
                // so we wait a cerains amount of time before requesting again
                if (retry > 3) {
                    return pagerList;
                }
                Thread.sleep(5000);
                retry++;
                getJSONParserList(url, after, language);

            } else {

                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }


        } catch (JSONException e) {
            // find a better way to end with trhe recursivity
            //here we stop because there is no next page
            return pagerList;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return pagerList;
    }

    /**
     * help to get the url of the page to contact
     *
     * @param url   : represente a given url of the page to contact
     * @param after : specify a date from which a repo can be recover
     * @return
     */
    private String constructURL(String url, String after) {
        if (url.equals(this.bitbucketAPItURL)) {
            String suffix = "repositories/";
            if (after != null && !after.isEmpty()) {
                suffix += "?after=" + after;
            }
            return bitbucketAPItURL + suffix;
        }
        return url;
    }


    public List<JSONRepoParser> getJSONParserList(String after, String language) throws IOException, ParseException {
        return getJSONParserList(this.bitbucketAPItURL, after, language);
    }

    public static void main(String[] args) throws IOException, ParseException {
        BitbucketTracker bitbucketTracker = new BitbucketTracker();
        List<JSONRepoParser> java = bitbucketTracker.getJSONParserList("2015-07-01", "java");
        System.out.println(java.size());
    }
}
