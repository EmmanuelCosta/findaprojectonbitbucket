package bitbucket.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebabalac on 07/07/2015.
 */
public class Language {


    public static List<String> getLanguages(String prefixeWord){
        List<String> languages = new ArrayList<String>();
        prefixeWord=prefixeWord.toLowerCase();
        for(LanguagesEnum langs : LanguagesEnum.values()){
            String value = langs.getValue();
            value= value.toLowerCase();
            if(value.startsWith(prefixeWord)){
                languages.add(value);
            }
        }
        return languages;
    }


}
