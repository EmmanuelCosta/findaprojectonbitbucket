package bitbucket.servlet;

import bitbucket.Utils.JSONUtils;
import bitbucket.model.Language;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by ebabalac on 07/07/2015.
 */
@WebServlet(name = "AutoCompleteLanguageServlet", urlPatterns = "/AutoCompleteLanguageServlet/*")
public class AutoCompleteLanguageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

       String prefixeWord= req.getParameter("term");
        Logger.getAnonymousLogger().info(prefixeWord);
        List<String> languages = Language.getLanguages(prefixeWord);
        String JSONLang = JSONUtils.toJSON(languages);

        PrintWriter writer = resp.getWriter();
        writer.println(JSONLang);
        writer.close();
        resp.setStatus(HttpServletResponse.SC_OK);
    }
}
