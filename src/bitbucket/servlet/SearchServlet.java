package bitbucket.servlet;

import bitbucket.parser.JSONRepoParser;
import bitbucket.model.tracker.BitbucketTracker;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by emmanuel on 01/07/2015.
 */
@WebServlet(name = "SearchServlet", urlPatterns = "/SearchServlet/*")
public class SearchServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public SearchServlet() {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            String language = request.getParameter("language");
            String startdate = request.getParameter("startdate");
            BitbucketTracker bitbucketTracker = new BitbucketTracker();
            List<JSONRepoParser> jsonParserList = bitbucketTracker.getJSONParserList(startdate,language);
            String json = "";
            int j=0;
            JSONObject jsonObject = new JSONObject();
            for (JSONRepoParser jsonRepoParser : jsonParserList) {
                JSONObject json1 = jsonRepoParser.getJSON();
                JSONArray reposValues = (JSONArray) json1.get("repoValues");
                System.out.println("reposValues : "+reposValues);
//                json = jsonRepoParser.toJSONString();
//                JSONArray jsonArray = new JSONArray();
//                jsonArray.put(json);
//                jsonObject.put("values "+i,jsonArray);

//                jsonObject.append("Repos",json);
                System.out.println(reposValues.length());
                for (int  i = 0; i < reposValues.length(); i++) {
                    JSONObject jsonObject1 = reposValues.getJSONObject(i);
                    jsonObject.append("Repos",jsonObject1);
                    j++;
                }
            }
            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_OK);
            Logger.getAnonymousLogger().info("RESULT :  "+j+"  " + jsonObject.toString());
            PrintWriter out = response.getWriter();
            out.println(jsonObject.toString());
            out.close();

        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
