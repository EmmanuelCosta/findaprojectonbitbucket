package bitbucket.Utils;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by ebabalac on 07/07/2015.
 */
public class JSONUtils {

    public  static String toJSON(List<String> elts) {
        Objects.requireNonNull(elts);
        JSONArray array = new JSONArray();

        array.put(elts);

        return array.toString();
    }

//    public static void main(String[] args) {
//        List<String> str = new ArrayList<String>();
//        str.add("toto");
//        str.add("momo");
//        str.add("mumu");
//
//    }
}
