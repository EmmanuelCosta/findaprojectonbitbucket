package bitbucket.parser;

import bitbucket.model.Language;
import bitbucket.model.LanguagesEnum;
import bitbucket.model.tracker.Repo;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

/**
 * Created by emmanuel on 26/06/2015.
 */
public class JSONRepoParser {

    private String nextPage;
    private Integer currentPage;
    private final List<Repo> repoList;
    private String language;

    public JSONRepoParser() {
        this.repoList = new ArrayList<Repo>();
        this.language = LanguagesEnum.ALL.getValue();
    }

    public void parse(String jsonToParse) throws ParseException {
        Objects.requireNonNull(jsonToParse);
        construct(jsonToParse);
    }

    public void parse(String jsonToParse, String language) throws ParseException {
        Objects.requireNonNull(jsonToParse);
        Objects.requireNonNull(language);
        setLanguage(language);
        parse(jsonToParse);
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    private void construct(String jsonToParse) throws ParseException {
        JSONObject obj = new JSONObject(jsonToParse);
//        System.out.println(jsonToParse);
        //get the url of the next page
        nextPage = obj.getString("next");
        currentPage = obj.getInt("page");
//        System.out.println(currentPage);
        JSONArray values = obj.getJSONArray("values");
        for (int i = 0; i < values.length(); i++) {
            Repo repo = new Repo();
            JSONObject jsonObject = values.getJSONObject(i);
            String language = jsonObject.getString("language");

            if (!this.language.equalsIgnoreCase("ALL") && !language.equalsIgnoreCase(this.language)) {
                continue;
            }
            String reponame = jsonObject.getString("name");
            JSONObject links = jsonObject.getJSONObject("links");

            JSONObject html = links.getJSONObject("html");
            String href = html.getString("href");

            JSONObject owner = jsonObject.getJSONObject("owner");
            String username = owner.getString("username");
            String update = jsonObject.getString("updated_on");
            String create = jsonObject.getString("created_on");
            String description = jsonObject.getString("description");

            SimpleDateFormat bitBucketDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date createDate = bitBucketDateFormat.parse(create);
            Date updateDate = bitBucketDateFormat.parse(update);

            repo.setCreate_on(createDate);
            repo.setDescription(description);
            repo.setLanguage(language);
            repo.setLast_update(updateDate);
            repo.setLinkToRepo("");
            repo.setRepoName(reponame);
            repo.setOwner(username);
            repo.setLinkToRepo(href);
            // System.out.println(reponame+" "+href+ " "+language+" "+username+" "+update+" "+updateDate+"  "+description+" "+create+" "+createDate);
            repoList.add(repo);
        }


    }

    public String toJSONString() {
        return getJSON().toString();
    }

    public JSONObject getJSON() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageLen", repoList.size());
        jsonObject.put("nextPage", nextPage);
        jsonObject.put("currentPage", currentPage);

        for (Repo repo : repoList) {
            JSONObject array = new JSONObject();

            array.put("language", repo.getLanguage());
            array.put("created_on", repo.getCreate_on());
            array.put("last_update", repo.getLast_update());
            array.put("description", repo.getDescription());
            array.put("links", repo.getLinkToRepo());
            array.put("owner", repo.getOwner());
            array.put("repoName", repo.getRepoName());
            jsonObject.append("repoValues", array);
        }
        return jsonObject;
    }

    public List<Repo> getRepoList() {
        return repoList;
    }

    public String getNextPage() {
        return nextPage;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }
}
